import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
# import  as u
import utils.perspectiveChange as utility
import os
x = 0
y = 0
width = 0
height = 0
#template matching in opencv
img_rgb = cv.imread(r"D:\PythonProjects\golfdaddy_divot\trainingimages\730.jpg")

template = cv.imread(os.path.join('./queryImages',"queryImage6.jpg"), cv.IMREAD_GRAYSCALE)
w, h = template.shape[::-1]
print(w,h)
img_gray = cv.cvtColor(img_rgb, cv.COLOR_BGR2GRAY)
res = cv.matchTemplate(img_gray,template,cv.TM_CCOEFF_NORMED)
threshold = 0.7
loc = np.where(res >= threshold)
for pt in zip(*loc[::-1]):
    # cv.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
    x = pt[0]
    y = pt[1]
    width = w
    height = h
    # img_rgb = cv.resize(img_rgb,(580,920))
    # cv.imshow("frame", img_rgb)
    crop_img = img_rgb[y:y+height, x:x+width] 
    cv.imshow("img1.jpg",crop_img)
cv.waitKey(0)
