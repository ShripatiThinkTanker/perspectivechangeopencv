import cv2
import numpy as np
def changePerspective(pts):
    # Turn on Laptop's webcam
    cap = cv2.imread("D:\PythonProjects\golfdaddy_divot\img1.jpg")

    pts_y1 = pts["y"] + pts["h"]
    pts_x1 = pts["x"] + pts["w"]
    pts1 = np.float32([[15,4],[8,147],[331,132],[331,1]])
    pts2 = np.float32([[0,0],[450,0],[0,350],[450,350]])
        
    # Apply Perspective Transform Algorithm
    matrix = cv2.getPerspectiveTransform(pts1, pts2)
    result = cv2.warpPerspective(cap, matrix, (600, 600))
        
    # Wrap the transformed image
    cv2.imshow('frame', cap) # Initial Capture
    cv2.imshow('frame1.jpg', result) # Transformed Capture

    cv2.waitKey(0)
    cv2.destroyAllWindows()